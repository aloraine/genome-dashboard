const express = require('express');
const router = express.Router();
const fs = require('fs');
const getContent = require('../utility/util.js');
const path = require('path');

let getImages = async () => {
  let imageList = [];
  fs.readdirSync(path.join(__dirname , '../public/images')).forEach(file => {
    imageList.push(file);
  });
  return imageList;
};

/* GET home page. */
router.get('/',  async (req, res) => {
  let content = await getContent.getContent();
  let listImage = await getImages();
  if (content) {
    res.render('index', {data: content, images: listImage});
  }
});

module.exports = router;
